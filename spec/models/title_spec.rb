require 'rails_helper'

RSpec.describe Title, type: :model do
  it { should validate_presence_of(:name) }
end
