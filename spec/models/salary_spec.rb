require 'rails_helper'

RSpec.describe Salary, type: :model do
  it { should validate_presence_of(:employee_id) }
  it { should validate_presence_of(:amount) }
end
