require 'rails_helper'

RSpec.describe DeptEmp, type: :model do
  it { should belong_to(:employee).dependent(:destroy) }
  it { should belong_to(:department).dependent(:destroy) }
  # Validation tests
  it { should validate_presence_of(:employee_id) }
  it { should validate_presence_of(:department_id) }
end
