require 'rails_helper'

RSpec.describe Employee, type: :model do
  it { should have_many(:salaries).dependent(:destroy) }
  # Validation tests
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:hire_date) }
end
