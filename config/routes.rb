Rails.application.routes.draw do
  namespace :api do
	namespace :v0 do
		resources :employees
		resources :salaries
		resources :departments
		resources :titles
		resources :dept_managers
	end
  end  
end  
