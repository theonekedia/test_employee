module Response
  def json_response(object, status = :ok)
  	response = Hash.new
  	response['data'] = object
    render json: response, status: status
  end
end