class DeptEmp < ApplicationRecord
	belongs_to :department, dependent: :destroy
	belongs_to :employee, dependent: :destroy
	validates_presence_of :employee_id, :department_id
end
