class DeptManager < ApplicationRecord
	belongs_to :employee
	belongs_to :department
	validates_presence_of :employee_id, :department_id
end
