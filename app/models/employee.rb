class Employee < ApplicationRecord
	tomorrow = Date.tomorrow
	scope :current_title, -> {joins(:titles).where('titles.to_date > ? OR titles.to_date IS NULL', tomorrow)}
	scope :current_department, -> {joins(:dept_emps).joins(:departments).where('dept_emps.to_date > ? OR dept_emps.to_date IS NULL', tomorrow)}
	scope :current_salary, -> {joins(:salaries).where('salaries.to_date > ? OR salaries.to_date IS NULL', tomorrow)}
	has_many :dept_emps
	has_many :salaries, dependent: :destroy
	has_many :departments, through: :dept_emps
	has_many :titles
	has_many :dept_managers

	validates_presence_of :first_name, :last_name, :hire_date

	def self.compact_details
		Employee.current_department.current_salary.current_title.map{|emp| 
			{
				id: emp.id,
				name: emp.first_name + " " + emp.last_name,
				gender: emp.gender,
				birthday: emp.birthday,
				salary: emp.salaries.map{|sal| {amount: sal.amount, from_date: sal.from_date}},
				department: emp.dept_emps.map{|dept| {name: dept.department.name, from_date: dept.from_date, manager: dept.department.dept_managers.first.employee.first_name }},
				title: emp.titles.map{|title| {name: title.name, from_date: title.from_date, to_date: title.to_date || ''}}
			}
		}
	end

	def self.compact_detail id
		Employee.current_department.current_salary.current_title.where(id: id).select('
			employees.id, employees.first_name, employees.gender, employees.birthday,
				salaries.amount as salary, salaries.from_date as salary_from_date,
				departments.name as department_name, dept_emps.from_date as department_from_date,
				titles.name as title_name, titles.from_date as title_from_date')
	end
end
