class Department < ApplicationRecord
	scope :current_manager, -> {joins(:dept_managers).where('dept_managers.to_date =?', nil)}
	has_many :dept_emp
	has_many :employees, through: :dept_emp
	has_many :dept_managers
	
	validates_presence_of :name
end
