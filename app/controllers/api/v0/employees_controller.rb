module Api
module V0
  class EmployeesController < ApiController
  before_action :set_employee, except: [:index]

  # GET /employees
  def index
    json_response(Employee.compact_details)
  end

  # GET /employees/:id
  def show
    result = Employee.compact_detail(params[:id])
    json_response(result)
  end

  # POST /employee
  def create
    @employee.create!(employee_params)
    json_response(@employee, :created)
  end

  # PUT /employees/:id
  def update
    @employee.update(employee_params)
    head :no_content
  end

  # DELETE /employees/:id
  def destroy
    @employee.destroy
    head :no_content
  end

  private

  def employee_params
    params.permit(:first_name, :last_name, :birthday, :hire_date, :gender)
  end

  def set_employee
    @employee = Employee.find(params[:id])
  end
end
end
end