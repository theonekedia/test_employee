module Api
module V0
  class TitlesController < ApiController
  before_action :set_title, except: [:index]

  # GET /titles
  def index
    json_response(Title.all)
  end

  # GET /titles/:id
  def show
    json_response(@title)
  end

  # POST /title
  def create
    @title.create!(title_params)
    json_response(@title, :created)
  end

  # PUT /titles/:id
  def update
    @title.update(title_params)
    head :no_content
  end

  # DELETE /titles/:id
  def destroy
    @title.destroy
    head :no_content
  end

  private

  def title_params
    params.permit(:name, :employee_id, :from_date, :to_date)
  end

  def set_title
    @title = Title.find(params[:id])
  end

end
end
end