module Api
module V0
  class DepartmentsController < ApiController
  before_action :set_department, except: [:index]

  # GET /departments
  def index
    json_response(Department.all)
  end

  # GET /departments/:department_id
  def show
    json_response(@department)
  end

  # POST /department
  def create
    @department.create!(department_params)
    json_response(@department, :created)
  end

  # PUT /departments/:department_id
  def update
    @department.update(department_params)
    head :no_content
  end

  # DELETE /departments/:department_id
  def destroy
    @department.destroy
    head :no_content
  end

  private

  def department_params
    params.permit(:name)
  end

  def set_department
    @department = Department.find(params[:id])
  end

end
end
end