module Api
module V0
  class DeptManagersController < ApiController
  before_action :set_dept_manager, except: [:index]

  # GET /dept_managers
  def index
    json_response(DeptManager.all)
  end

  # GET /dept_managers/:id
  def show
    json_response(@dept_manager)
  end

  # POST /dept_manager
  def create
    @dept_manager.create!(dept_manager_params)
    json_response(@dept_manager, :created)
  end

  # PUT /dept_managers/:id
  def update
    @dept_manager.update(dept_manager_params)
    head :no_content
  end

  # DELETE /dept_managers/:id
  def destroy
    @dept_manager.destroy
    head :no_content
  end

  private

  def dept_manager_params
    params.permit(:employee_id, :department_id, :from_date, :to_date)
  end

  def set_dept_manager
    @dept_manager = DeptManager.find(params[:id])
  end

end
end
end