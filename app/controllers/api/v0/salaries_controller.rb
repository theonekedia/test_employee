module Api
module V0
  class SalariesController < ApiController
  before_action :set_salary, except: [:index]

  # GET /salarys
  def index
    json_response(Salary.all)
  end

  # GET /salarys/:id
  def show
    json_response(@salary)
  end

  # POST /salary
  def create
    @salary.create!(salary_params)
    json_response(@salary, :created)
  end

  # PUT /salarys/:id
  def update
    @salary.update(salary_params)
    head :no_content
  end

  # DELETE /salarys/:id
  def destroy
    @salary.destroy
    head :no_content
  end

  private

  def salary_params
    params.permit(:employee_id, :amount, :from_date, :to_date)
  end

  def set_salary
    @salary = Salary.find(params[:id])
  end

end
end
end