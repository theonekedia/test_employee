# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

department = Department.find_or_initialize_by(name: "Technology")
if department.new_record?
	department.save!
else
	puts "department exists"
end
puts "Finished Department"

employee = Employee.find_or_initialize_by(first_name: 'Goldie', last_name: "Kedia", birthday: "10/04/1988", hire_date: "10/10/2018", gender: "Male")
if employee.new_record?
	employee.save!
	salary = Salary.find_or_initialize_by(employee_id: employee.id, amount: 125000, from_date: "10/10/2018", to_date: "10/12/2018")
	if salary.new_record?
		salary.save!
	else
		puts "salary exists"
	end
	puts "Finished Salary"
	dept_emp = DeptEmp.find_or_initialize_by(employee_id:employee.id ,department_id: 1, from_date: "10/10/2018")
	dept_emp.save!
else
	puts "employee exists"
end
puts "Finished Employee"

employee = Employee.find_or_initialize_by(first_name: 'Akash', last_name: "Patil", birthday: "29/04/1987", hire_date: "10/10/2016", gender: "Male")
if employee.new_record?
	employee.save!
	salary = Salary.find_or_initialize_by(employee_id: employee.id, amount: 250000, from_date: "10/10/2018", to_date: "10/12/2018")
	if salary.new_record?
		salary.save!
	else
		puts "salary exists"
	end
	puts "Finished Salary"
	dept_emp = DeptEmp.find_or_initialize_by(employee_id:employee.id,department_id: 1, from_date: "10/10/2018")
	dept_emp.save!
else
	puts "employee exists"
end
puts "Finished Employee"


titles = [{name: 'Head Of Technology', id: 1},{name: "CEO", id: 2}]
titles.each do |c|
	title = Title.find_or_initialize_by(name: c[:name])
	if title.new_record?
		title.employee_id = c[:id]
		title.from_date = "10/10/2018"
		title.save!
	else
		puts "#{c[:name]} exists"
	end
end
puts "Finished Titles"

dept_manager = DeptManager.find_or_initialize_by(employee_id: 2, department_id: 1, from_date: '10/10/2016')
if dept_manager.new_record?
	dept_manager.save!
else
	puts "dept_manager exists"
end
puts "Finished Dept Manager"
Title.find_or_initialize_by(name: "Senior Software Engineer", employee_id: 1, from_date: "10/01/2018", to_date: "01/10/2018").save!
