class CreateSalaries < ActiveRecord::Migration[5.2]
  def change
    create_table :salaries do |t|
      t.integer :employee_id
      t.integer :amount
      t.date :from_date
      t.date :to_date

      t.timestamps
    end
  end
end
