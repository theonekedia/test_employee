class CreateDeptManagers < ActiveRecord::Migration[5.2]
  def change
    create_table :dept_managers do |t|
      t.references :employee, froeign_key: true
      t.references :department, froeign_key: true
      t.date :from_date
      t.date :to_date

      t.timestamps
    end
  end
end
