class CreateTitles < ActiveRecord::Migration[5.2]
  def change
    create_table :titles do |t|
      t.string :name
      t.references :employee, froeign_key: true
      t.date :from_date
      t.date :to_date

      t.timestamps
    end
  end
end
